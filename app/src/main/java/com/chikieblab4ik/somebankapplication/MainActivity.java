package com.chikieblab4ik.somebankapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.chikieblab4ik.somebankapplication.ui.dashboard.DashboardFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "belarus";
    private EditText editTextLogin;
    private EditText editTextPassword;
    private Button regButton;
    String token;
    String loginFromET; // логин из поля ввода логина
    String passwordFromET; // пароль из поля ввода пароля
    String[] keys;
    String[] values;

    String url = "https://reqres.in/api/login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        editTextLogin = findViewById(R.id.etLogin);
        editTextPassword = findViewById(R.id.etPassword);
        regButton = findViewById(R.id.btnReg);

//        clickBtn();
        clickBtnohohhohohoMneLen();
    }

    public void fillArrays() {
        keys = new String[]{"email", "password"};
        values = new String[]{"q", "w"}; // eve.holt@reqres.in &&& cityslicka
    }

    public void clickBtn() {
        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fillArrays();
                token = Api.post(keys, values, url); // строчка с токеном

                loginFromET = editTextLogin.getText().toString();
                passwordFromET = editTextPassword.getText().toString();
                if (loginFromET.equals(values[0]) & passwordFromET.equals(values[1])) {
                    Intent intent = new Intent(MainActivity.this, BottomNavigationActivity.class);
                    intent.putExtra("token", token); // по заданию он не нужен, но пусть
                    startActivity(intent);
                } else if (loginFromET.equals("") & passwordFromET.equals("")) {
                    Toast.makeText(MainActivity.this, "А где", Toast.LENGTH_SHORT).show();
                } else if (loginFromET != "" & passwordFromET.equals("")){
                    Toast.makeText(MainActivity.this, "Эм а где пароль????", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(MainActivity.this, "Мдааа чел введи правельные данные", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void clickBtnohohhohohoMneLen() {
        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(MainActivity.this, BottomNavigationActivity.class);
                    intent.putExtra("token", token); // по заданию он не нужен, но пусть
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "onClick: " + e);
                }
            }
        });
    }
}