package com.chikieblab4ik.somebankapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity extends AppCompatActivity {

    private TextView btnChPass;
    private TextView btnChLog;
    private TextView btnHistory;
    private TextView btnInfo;
    private TextView btnLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().hide();
        btnChLog = findViewById(R.id.btnChLog);
        btnChPass = findViewById(R.id.btnChPass);
        btnHistory = findViewById(R.id.btnHistory);
        btnInfo = findViewById(R.id.btnInfo);
        btnLogout = findViewById(R.id.btnLogout);

        final AlertDialog.Builder builder1 = new AlertDialog.Builder(getApplicationContext());
        builder1.setTitle("Всем привет я марина ауе");
        builder1.setMessage("А тут снизу короче надпись всем барашек?");

        builder1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(ProfileActivity.this, "123123123123", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(ProfileActivity.this, "NOOOOOOOOOOOOOOOOOOOOOOOOOO", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        btnChLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                builder1.show();
            }
        });

        btnChPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, HistoryVisitsActivity.class);
                startActivity(intent);
            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }
}