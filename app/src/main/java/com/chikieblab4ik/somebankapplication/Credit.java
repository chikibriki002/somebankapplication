package com.chikieblab4ik.somebankapplication;

public class Credit {

    private String payDate;
    private String typeCredit;
    private double sumEveryMonthPay;
    private double sumCredit;

    public Credit(String typeCredit, String payDate, double sumEveryMonthPay) {
        this.typeCredit = typeCredit;
        this.payDate = payDate;
        this.sumEveryMonthPay = sumEveryMonthPay;
    }

    public String getPayDate() {
        return payDate;
    }

    public String getTypeCredit() {
        return typeCredit;
    }

    public double getSumEveryMonthPay() {
        return sumEveryMonthPay;
    }

    public double getSumCredit() {
        return sumCredit;
    }
}
