package com.chikieblab4ik.somebankapplication.ui.dialogs;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DialogsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public DialogsViewModel() {
        mText = new MutableLiveData<>();
    }

    public LiveData<String> getText() {
        return mText;
    }
}