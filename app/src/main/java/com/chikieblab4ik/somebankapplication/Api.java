package com.chikieblab4ik.somebankapplication;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

public class Api {

    private static final String TAG = "belarus";

    public Api() {}

    public static HttpURLConnection startConnection(String url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            return connection;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(MainActivity.TAG, "startConnection: " + e);
            return null;
        }
    }

    public static String responseFromStream(BufferedReader in) {
        try {
            StringBuilder constructor = new StringBuilder();
            String temp;
            while ((temp = in.readLine()) != null) {
                constructor.append(temp);
            }
            return constructor.toString();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "makeStrungUrl: " + e);
            return null;
        }
    }

    static String rezult;

    public static String get(final String url) {
        rezult = null;
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = startConnection(url);
                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    rezult =  responseFromStream(in);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "Run GET: " + e);
                }
            }
        };

        Thread th = new Thread(run);
        try {
            th.start();
            th.join();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "Thread GET: " + e);
        }
        return rezult;
    }

    public static String makeLink(String[] keys, String[] values) {
        StringBuilder constructor = new StringBuilder();
        for (int i = 0; i < keys.length; i++) {
            constructor.append(keys[i]);
            constructor.append("=");
            constructor.append(values[i]);
            constructor.append("&");
        }
        return constructor.substring(0, constructor.length() - 1);
    }

    public static String post(final String[] keys, final String[] values, final String url) {
        rezult = null;
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = startConnection(url);

                    String link = makeLink(keys, values);

                    connection.setRequestMethod("POST");
                    connection.setDoOutput(true);
                    connection.getOutputStream().write(link.getBytes());

                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    rezult = responseFromStream(in);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        Thread th2 = new Thread(run);
        try {
            th2.start();
            th2.join();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "Thread POST: " + e);
        }
        return rezult;
    }
}
