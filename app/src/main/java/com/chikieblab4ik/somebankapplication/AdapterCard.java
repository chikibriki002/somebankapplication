package com.chikieblab4ik.somebankapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterCard extends BaseAdapter {

    Context ctx;
    LayoutInflater inflater;
    ArrayList<Card> cardsArray;

    AdapterCard(Context context, ArrayList<Card> cardsArrayList) {
        ctx = context;
        cardsArray = cardsArrayList;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return cardsArray.size();
    }

    @Override
    public Object getItem(int i){
        return cardsArray.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View viewC, ViewGroup viewGroup) {
        View view = viewC;
        if (view == null) {
            view = inflater.inflate(R.layout.item_card, viewGroup, false);
        }
        Card oneCard = cardsArray.get(i);

        StringBuilder constructor = new StringBuilder();
        constructor.append(oneCard.getCardNumber());
        String numberCard = constructor.replace(4, 8, "****").toString();

        ((TextView) (view).findViewById(R.id.tvTypeCredit)).setText(oneCard.getTypeCard());
        ((TextView) (view).findViewById(R.id.tvNum)).setText(numberCard);
        ((TextView) (view).findViewById(R.id.tvMoney)).setText(oneCard.getMoneyOnCard() + "");
        ((view).findViewById(R.id.imgCard)).setBackgroundResource(oneCard.getCardImg());
        return view;
    }
}
