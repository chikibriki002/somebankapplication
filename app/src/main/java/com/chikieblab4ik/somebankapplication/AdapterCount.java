package com.chikieblab4ik.somebankapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class AdapterCount extends BaseAdapter {

    Context ctx;
    LayoutInflater inflater;
    ArrayList<Count> countsArray;

    AdapterCount(Context context, ArrayList<Count> countsArrayList) {
        ctx = context;
        countsArray = countsArrayList;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return countsArray.size();
    }

    @Override
    public Object getItem(int i) {
        return countsArray.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View viewC, ViewGroup viewGroup) {
        View view = viewC;
        if (view == null) {
            view = inflater.inflate(R.layout.item_count, viewGroup, false);
        }
        Count oneCount = countsArray.get(i);

        ((TextView) (view).findViewById(R.id.tvMoneyCount)).setText(oneCount.getMoneyOnCount() + "");
        ((TextView) (view).findViewById(R.id.tvNumCount)).setText(oneCount.getNumCount());
        return view;
    }
}
