package com.chikieblab4ik.somebankapplication;

public class Count {

    private String numCount;
    private double moneyOnCount;

    public Count(String numCount, double moneyOnCount) {
        this.numCount = numCount;
        this.moneyOnCount = moneyOnCount;
    }

    public String getNumCount() {
        return numCount;
    }

    public double getMoneyOnCount() {
        return moneyOnCount;
    }
}
