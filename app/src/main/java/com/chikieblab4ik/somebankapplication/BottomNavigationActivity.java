package com.chikieblab4ik.somebankapplication;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import org.json.JSONObject;

import java.util.ArrayList;

public class BottomNavigationActivity extends AppCompatActivity {

    public static final String TAG = "vilka";

    private TextView textName;
    private ListView lvCards;
    private ListView lvCounts;
    private ListView lvCredits;
    private ImageView profileImg;

    ArrayList<CardAPI> cardsAPIArray;
    ArrayList<CountAPI> countsAPIArray;
    ArrayList<CreditAPI> creditsAPIArray;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);
        getSupportActionBar().hide();
        BottomNavigationView navView = findViewById(R.id.nav_view);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_pay, R.id.navigation_history, R.id.navigation_dialogs).build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        textName = findViewById(R.id.tvName);
        lvCards = findViewById(R.id.lvCards);
        lvCounts = findViewById(R.id.lvAccounts);
        lvCredits = findViewById(R.id.lvCredits);
        profileImg = findViewById(R.id.imgProfile);

        profileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BottomNavigationActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        Log.i(TAG, "onCreate:  ЗАПУСУКАУКШЩЗРПКШЩРПУКФШРХЩ");

        createNameMiddleName(textName);
        createData();

        ArrayList<Card> finalCardsArray = createArrayCards();
        AdapterCard adapterCard = new AdapterCard(this, finalCardsArray);
        lvCards.setAdapter(adapterCard);

        ArrayList<Count> finalCountsArray = createArrayCounts();
        AdapterCount adapterCount = new AdapterCount(this, finalCountsArray);
        lvCounts.setAdapter(adapterCount);

        ArrayList<Credit> finalCreditsArray = createArrayCredits();
        AdapterCredit adapterCredit = new AdapterCredit(this, finalCreditsArray);
        lvCredits.setAdapter(adapterCredit);


    }

//    <-------------------------------------------------------------------------------->

    public Card createOneCard(CardAPI cardAPI) {
        try {
            String typeCard = cardAPI.getTypeCardA();
            String cardNumber = cardAPI.getCardNumberA();
            int cardImg = cardAPI.getCardImgA();
            double moneyOnCard = cardAPI.getMoneyOnCardA();
            Card card = new Card(typeCard, cardNumber, cardImg, moneyOnCard);
            return card;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "createOneCard: " + e);
            return null;
        }
    }

    public ArrayList<Card> createArrayCards() {
        try {
            ArrayList<Card> cardArrayList = new ArrayList<>();
            for (int i = 0; i < cardsAPIArray.size(); i++) {
                Card card = createOneCard(cardsAPIArray.get(i));
                cardArrayList.add(card);
            }
            return cardArrayList;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "createArrayCards: " + e);
            return null;
        }
    }

//    <-------------------------------------------------------------------------------->

    public Count createOneCount(CountAPI countAPI) { // метод для создания объекта Card, в котором будут значения из json (но у меня нету api с картами)
        try {
            String numCount = countAPI.getNumCountA();
            double moneyOnCount = countAPI.getMoneyOnCountA();
            Count count = new Count(numCount, moneyOnCount);
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "createOneCard: " + e);
            return null;
        }
    }

    public ArrayList<Count> createArrayCounts() {
        try {
            ArrayList<Count> countArrayList = new ArrayList<>();
            for (int i = 0; i < countsAPIArray.size(); i++) {
                Count count = createOneCount(countsAPIArray.get(i));
                countArrayList.add(count);
            }
            return countArrayList;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "createArrayCounts: " + e);
            return null;
        }
    }

//    <-------------------------------------------------------------------------------->

    public Credit createOneCredit(CreditAPI creditAPI) {
        try {
            String payDate = creditAPI.getPayDateA();
            String typeCredit = creditAPI.getTypeCreditA();
            double sumEveryMonthPay = creditAPI.getSumEveryMonthPayA();
            Credit credit = new Credit(typeCredit, payDate, sumEveryMonthPay);
            return credit;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "createOneCard: " + e);
            return null;
        }
    }

    public ArrayList<Credit> createArrayCredits() {
        try {
            ArrayList<Credit> creditArrayList = new ArrayList<>();
            for (int i = 0; i < creditsAPIArray.size(); i++) {
                Credit credit = createOneCredit(creditsAPIArray.get(i));
                creditArrayList.add(credit);
            }
            return creditArrayList;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "createArrayCredits: " + e);
            return null;
        }
    }

//    <-------------------------------------------------------------------------------->




    public void createData() {
        cardsAPIArray = new ArrayList<>();
        cardsAPIArray.add(new CardAPI("Дебетовая карта", "123456654321", R.drawable.card, 4509.95));
        cardsAPIArray.add(new CardAPI("Кредитная карта", "123456654321", R.drawable.card, 450000.00));
        cardsAPIArray.add(new CardAPI("Дебетовая карта", "123456654321", R.drawable.card, 0.12));
        cardsAPIArray.add(new CardAPI("Дебетовая карта", "123456654321", R.drawable.card, 10000.00));

        countsAPIArray = new ArrayList<>();
        countsAPIArray.add(new CountAPI("12341324132456", 12345.56));
        countsAPIArray.add(new CountAPI("12341324132456", 150.00));
        countsAPIArray.add(new CountAPI("12341324132456", 10000.00));
        countsAPIArray.add(new CountAPI("12341324132456", 400.00));

        creditsAPIArray = new ArrayList<>();
        creditsAPIArray.add(new CreditAPI("Кредит наличными", "26.01.18", 10000.00));
        creditsAPIArray.add(new CreditAPI("Ипотека", "14.12.20", 10.00));


    }

    public static void createNameMiddleName(TextView textView) {
        try {
            String urlWithJSON = "https://reqres.in/api/users/2";
            String url = new Api().get(urlWithJSON);
            JSONObject obj = new JSONObject(url); // весь json
            JSONObject jsonObject = (JSONObject) obj.get("data"); // data
            String first_nameJs = jsonObject.getString("first_name");
            String last_nameJs = jsonObject.getString("last_name");
            textView.setText(first_nameJs + " " + last_nameJs);
            Log.i(TAG, "createNameMiddleName: " + first_nameJs + " " + last_nameJs);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "onCreate: " + e);
        }
    }
}

class CardAPI {

    public String typeCardA;
    public String cardNumberA;
    public int cardImgA;
    public double moneyOnCardA;

    public CardAPI(String typeCard, String cardNumber, int cardImg, double moneyOnCard) {
        this.typeCardA = typeCard;
        this.cardNumberA = cardNumber;
        this.cardImgA = cardImg;
        this.moneyOnCardA = moneyOnCard;
    }

    public String getTypeCardA() {
        return typeCardA;
    }

    public String getCardNumberA() {
        return cardNumberA;
    }

    public int getCardImgA() {
        return cardImgA;
    }

    public double getMoneyOnCardA() {
        return moneyOnCardA;
    }
}

class CountAPI {

    private String numCountA;
    private double moneyOnCountA;

    public CountAPI(String numCount, double moneyOnCount) {
        this.numCountA = numCount;
        this.moneyOnCountA = moneyOnCount;
    }

    public String getNumCountA() {
        return numCountA;
    }

    public double getMoneyOnCountA() {
        return moneyOnCountA;
    }
}

class CreditAPI {

    private String payDateA;
    private String typeCreditA;
    private double sumEveryMonthPayA;
    private double sumCreditA;

    public CreditAPI(String typeCredit, String payDate, double sumEveryMonthPay) {
        this.typeCreditA = typeCredit;
        this.payDateA = payDate;
        this.sumEveryMonthPayA = sumEveryMonthPay;
    }

    public String getPayDateA() {
        return payDateA;
    }

    public String getTypeCreditA() {
        return typeCreditA;
    }

    public double getSumEveryMonthPayA() {
        return sumEveryMonthPayA;
    }

    public double getSumCreditA() {
        return sumCreditA;
    }
}

