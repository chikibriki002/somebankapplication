package com.chikieblab4ik.somebankapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterCredit extends BaseAdapter {

    Context ctx;
    LayoutInflater inflater;
    ArrayList<Credit> creditsArray;

    AdapterCredit(Context context, ArrayList<Credit> creditsArrayList) {
        ctx = context;
        creditsArray = creditsArrayList;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return creditsArray.size();
    }

    @Override
    public Object getItem(int i){
        return creditsArray.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View viewC, ViewGroup viewGroup) {
        View view = viewC;
        if (view == null) {
            view = inflater.inflate(R.layout.item_credit, viewGroup, false);
        }
        Credit oneCredit = creditsArray.get(i);

        ((TextView) (view).findViewById(R.id.tvTypeCredit)).setText(oneCredit.getTypeCredit());
        ((TextView) (view).findViewById(R.id.tvDateCredit)).setText(oneCredit.getPayDate());
        ((TextView) (view).findViewById(R.id.tvSumCredit)).setText(oneCredit.getSumEveryMonthPay() + "");
        return view;
    }
}