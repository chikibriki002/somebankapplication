package com.chikieblab4ik.somebankapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HistoryVisitsActivity extends AppCompatActivity {

    private ListView lvHistory;
    String url = "https://reqres.in/api/unknown";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_history_visits);
        lvHistory = findViewById(R.id.lvHistory);

        ArrayList<Visit> visitArrayList = createVisitsArray();
        AdapterHistory adapterHistory = new AdapterHistory(this, visitArrayList);
        lvHistory.setAdapter(adapterHistory);
    }

    public ArrayList<Visit> createVisitsArray() {
        ArrayList<Visit> visits = new ArrayList<>();
        try {
            String urlJSON = new Api().get(url);
            JSONObject jsonObject = new JSONObject(urlJSON);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                String date = jsonArray.getJSONObject(i).getString("year");
                String time = jsonArray.getJSONObject(i).getString("name");
                Visit visit = new Visit(date, time);
                visits.add(visit);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(BottomNavigationActivity.TAG, "parseJSON: " + e);
        }
        return visits;
    }

}