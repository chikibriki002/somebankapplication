package com.chikieblab4ik.somebankapplication;

import android.widget.ImageView;

public class Card {

    private String typeCard;
    private String cardNumber;
    private int cardImg;
    private double moneyOnCard;


    public Card(String typeCard, String cardNumber, int cardImg, double moneyOnCard) {
        this.typeCard = typeCard;
        this.cardNumber = cardNumber;
        this.cardImg = cardImg;
        this.moneyOnCard = moneyOnCard;
    }

    public String getTypeCard() {
        return typeCard;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public int getCardImg() {
        return cardImg;
    }

    public double getMoneyOnCard() {
        return moneyOnCard;
    }
}
