package com.chikieblab4ik.somebankapplication;

import java.util.ArrayList;

public class User {

    private String name;
    private String middleName;
    private String login;
    private String password;
    private ArrayList<Card> cards;
    private ArrayList<Count> counts;
    private ArrayList<Credit> credits;
    private ArrayList<Visit> visits;

    public User(String name, String middleName, String login, String password,
                ArrayList<Card> cards, ArrayList<Count> counts, ArrayList<Credit> credits, ArrayList<Visit> visits) {
        this.name = name;
        this.middleName = middleName;
        this.login = login;
        this.password = password;
        this.cards = cards;
        this.counts = counts;
        this.credits = credits;
        this.visits = visits;
    }

    public String getName() {
        return name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public ArrayList<Count> getCounts() {
        return counts;
    }

    public ArrayList<Credit> getCredits() {
        return credits;
    }

    public ArrayList<Visit> getVisits() {
        return visits;
    }
}
