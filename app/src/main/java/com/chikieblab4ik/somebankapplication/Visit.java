package com.chikieblab4ik.somebankapplication;

public class Visit {

    private String dateVisit;
    private String timeVisit;

    public Visit(String dateVisit, String timeVisit) {
        this.dateVisit = dateVisit;
        this.timeVisit = timeVisit;
    }

    public String getDateVisit() {
        return dateVisit;
    }

    public String getTimeVisit() {
        return timeVisit;
    }
}
