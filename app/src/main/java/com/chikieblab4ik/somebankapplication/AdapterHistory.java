package com.chikieblab4ik.somebankapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterHistory extends BaseAdapter {

    Context ctx;
    LayoutInflater inflater;
    ArrayList<Visit> visitArrayList;

    AdapterHistory(Context context, ArrayList<Visit> visitsArray) {
        ctx = context;
        visitArrayList = visitsArray;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return visitArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return visitArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.item_visit, parent, false);
        }

        Visit oneVisit = visitArrayList.get(position);

        ((TextView) (view).findViewById(R.id.tvDateVisit)).setText(oneVisit.getDateVisit());
        ((TextView) (view).findViewById(R.id.tvTimeVisit)).setText(oneVisit.getTimeVisit());

        return view;
    }
}
